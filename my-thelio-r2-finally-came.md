My Thelio R2 finally came

Disclaimer: I haven't been paid to provide this review. I am doing it because there aren't that many reviews for System76 products, including the normal Thelio.

Recently I ordered a Thelio computer with the following specs:

- AMD 3rd Gen Ryzen 5 3600X (3.8 up to 4.4 GHz - 6 Cores - 12 Threads) with PCIe 4.0
- 8GB of memory (I am going to buy more separately, as the price offered at the System76 webpage was oddly higher than the prices here)
- 120Gb SSD (Same situation as on memory)
- 8Gb Radeon RX 5700 XT with 2560 Stream Processors

The reason I went with something like System76 is because of their proposal of open source hardware. It is a bit expensive than building your own computer or even getting a Mac device with similar specs. Plus, at least here where I live (which is outside of US), PC builders are not used to create small computers, so it is almost impossible to find small cases. At least I was saving money to buy a new computer, as my Thinkpad T430 was showing its sign of age when running Packer.

## Unboxing

The good thing is that the packaging was a box inside another box. When I saw the courier guy delivering the box in the wrong way, I got scared for a while, but in the end, everything was fine.

![Full box](images/Thelio_review/2.jpg)
![Order information](images/Thelio_review/1.jpg)

Opening the box, the relief came and I saw the main box (the one with the product) surrounded by foam structures. This might be helpful to avoid any vibrations or such.

![Box inside box](images/Thelio_review/3.jpg)

Taking off the box from the external cardboard box, you can see a very nice packaging. Even a friend that studied Industrial Design praised it, which is cool. It shows the Open Source Hardware sign, plus a cool picture from the moon. Opening that box, I was greeted with a "Welcome to Thelio" message.

![Package external](images/Thelio_review/4.jpg)
![Package external 2](images/Thelio_review/5.jpg)
![Welcome to Thelio!](images/Thelio_review/6.jpg)

Now, you could see the top of the case and a postal card, which is a common gift from System76 with their products. I decided to lift the computer from the packaging and take more pictures of the welcome messages of the box.

![Top of case and postal card](images/Thelio_review/7.jpg)
![Welcome everywhere](images/Thelio_review/8.jpg)

## Product description 

I won't be detailed as I'm not a hardware expert, plus there might be a part 2 with the internals.

The case is beautiful. It has a side of wood on the right (real wood by the way) and a very detailed side on the left showing the Rocky Mountains from Colorado. On the front, you will find the System76 logo on the bottom and the power button above.

![Left side](images/Thelio_review/9.jpg)
![Right side](images/Thelio_review/A.jpg)

The back is where everything is located. In my case, on the top left, you can find four USB 3.2 Gen 2 ports (definitely the blue ones, and I suppose the white would be the fourth), a USB 3.2 Gen 2 Type-A (the red one), two DisplayPorts and a HDMI port. Also, there is a USB-C and an Ethernet port.

Below the middle section, you will find the Mic, Line out and Line in entrances (not marked, unfortunately), and, depending of your GPU, a section with the corresponding ports.

If you see the fan part, it has something like the solar system engraved on it (cool huh?).

![Back](images/Thelio_review/B.jpg)

Finally connecting the whole thing, everything run A-OK. You are greeted with a System76 boot screen (It uses Coreboot, double yay) and for the first set up, it will boot the installation of Pop OS!. Some people say they have heard the fan noisy when running the installation, but I heard nothing.

When I get into changing components, which would be in a few days, I can post its internals, so you can have a look at it.

## My complaints so far

Honestly, I have no complaints on the hardware part. Everything was recognized as expected, and as I said previously. I just want a computer and I don't want to nitpick on the hardware part because I really lack the knowledge on doing that right. Now, my nitpick is with Pop OS!. This will be mostly a complaint against Ubuntu and derivatives though, and it will deserve another post for sure.

When starting the OS, I was greeted with GNOME. I have been using GNOME for too long and I don't have any problems with it, specially because I use Fedora on my Thinkpad (which also uses GNOME). The shortcuts and whole functioning are totally different. Like, if I move the mouse to the top left corner, it doesn't show the application section. If I do `Super+Up`, it doesn't expand my screen. Damn, I really hope that won't be difficult to replicate, although I feel that shouldn't be the thing.

Then, when connecting my headphones, Pop OS! didn't detect it. I was pretty much worried, as I was thinking I needed to buy extras just to plug any analogic audio device on it. Luckily, it seems System76 have a [support page](https://support.system76.com/articles/audio/) on how to troubleshoot the audio issues. That's good, but it makes me think how incomplete is Pop OS! because needs to be tweaked to get something basic working, even though the same System76 is the one that develops it and, well, it came on one of their devices. It's like a Macbook not able to read any connected USB device and you need to do some tweaking to get that running.

I still need to play a little more with it, so this post might be filled of updates. I was thinking of getting NixOS on it (have been playing a lot with Nix and Home Manager), but honestly, I need a daily device with good support. I might try it on my laptop though, if my experience with OpenBSD fails miserably.

## UPDATE (21-10-2020)

Yesterday I had the chance to open the Thelio and add the RAM and extra storage to it. To the **My complaints so far** section, I want to add the lack of documentation for the thelio-r2. I ended up unmounting the GPU just to find there was already an NVM mounted there (not even mad, I thought it came with an SSD), but there was no second slot. By mere luck, I found it on the right side (the one covered with the wood panel).

![Left side opened](images/Thelio_review/D.jpg)
![Right side opened](images/Thelio_review/E.jpg)

I had a struggle with inserting the RAM, but it was my fault, as I'm not a careful person. I needed to accomodate a cable in front of the RAM, but no issues so far.
In the end, everything was recognized properly.

![System](images/Thelio_review/F.jpg)

Something good: I reinstalled Pop!_OS and it seems the sound fix does not need to be reproduced again. I also installed Nix with `home-manager` and got the `.desktop` files working, although I need to close session each time I install something new with Nix.

Tags: tech, unboxing, system76, review
