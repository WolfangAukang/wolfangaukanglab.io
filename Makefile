COMMIT_TIMESTAMP=date -u +%Y%m%d%H%M-

rebuild:
	podman run -it --rm --env-file=$(SETTINGS) -v ".:/content:Z" docker.io/wolfangaukang/bashblog bb rebuild 

post:
	podman run -it --rm --env-file=$(SETTINGS) -v ".:/content:Z" docker.io/wolfangaukang/bashblog bb post $(FILE)

post-direct:
	podman run -it --rm --env-file=$(SETTINGS) -v ".:/content:Z" docker.io/wolfangaukang/bashblog bb post -direct $(FILE)

edit:
	podman run -it --rm --env-file=$(SETTINGS) -v ".:/content:Z" docker.io/wolfangaukang/bashblog bb edit $(FILE)

git-push-entry:
	git add -A
	git commit -m "$(COMMIT_TIMESTAMP)$(MESSAGE)"
	git push
