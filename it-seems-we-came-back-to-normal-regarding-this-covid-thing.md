It seems we came back to normal regarding this COVID thing

This is kind of a COVID-19 post written on my perspective, so it is mandatory to add the disclaimer that this is my opinion and you should take it with a grain of salt. We can have a discussion on my email.

It seems everything came back to normal, as now I'm seeing too much people on the streets. I don't really care, as I got a (now completely) remote job and don't need to get out of the house unless I have to do something outside. And if I have to, I will use a mask (because they look cool and people won't see my face).

Now, I have to tell this: too many people are still romanticizing the situation thinking that we will learn something new from all of this. We didn't and we won't.

There is/are still:

- People still without jobs and suffering to survive
- People thinking that they are the center of the world, so if they don't like something, they should not feel obligated (even if it's a national mandate) to do it because it is their "personal freedom"®
- Even more conspiracy theories flowing on the net
- COVID (that's expected) and a bunch of people dying from it 
- And you pretty much know what I can add to this list, but it isn't necessary

All the people saying to prioritize the economy over health, now that they won, seems to have forgotten the ones in poverty even before the pandemic. Well, in the end, as long these people don't work for them, it doesn't really matter, they can die from COVID, poverty or whatever.

All the people that was ordering others to stay at home really ignored the fact that again, there is people who can't do it. They need food to survive, and on our current times, you need money even for vital needs, and that can only be obtained by work (unless the govt. is providing help to them).

Everyone seems to be enclosed in a bubble, in a need of dying it with a popular color so they can be part of its hivemind. And although this sounds like one of these sentences used by people that consider themselves "different than the others", now it seems you have to be part of something to have a valid opinion. If not, you are just a phony, or still an enemy.

I am just going to say it: There is something wrong with our current way of living. People can't live normally, we are fighting on banal topics, there is no common sense on basic considerations in a society. Now everything seems motivated by political intentions.

What I can just say is: Let's acknowledge this and see what we can improve personally (at least).

## TL;DR 

Whoever thought the world would change after COVID-19, needs some kind of reality check and understand the current battleground (yes, a battleground). But y'know, we can do something in the personal level to solve it. But we must acknowledge it first.

Tags: 2020, rant, covid, personal

